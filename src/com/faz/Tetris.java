package com.faz;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Tetris extends JFrame {
    private TCanvas canvas = new TCanvas(20, 12, 8);

    public static void main(String[] args) {
        new Tetris();
    }

    public Tetris() {
        initWindow();
        addComponents();
    }

    private void initWindow() {
        this.setSize(400,350);
        this.setTitle("My Tetris");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        this.setLayout(null);
    }
    private void addComponents() {
        this.add(canvas);
        canvas.setPosition(109, 20);
        canvas.setFocusable(true);
        canvas.addKeyListener(canvas);
		canvas.requestFocusInWindow();
        Thread canvasUpdateThread = new Thread(canvas);
        canvasUpdateThread.start();
    }



    class TCanvas extends JPanel implements KeyListener, Runnable {
        private int cellSize;
        private int xposition=0;
        private int yposition=0;
        private int rowCells;
        private int colCells;
        private int[][] data;
        private final int EMPTY = 0;
        private final int FILLED = 1;
        private int xFigureOffset = 0;
        private int yFigureOffset = 0;
        private TFigure currentTFigure;
        private boolean gameIsOver = false;
        private final int LOWER_TIME = 100;
        private final int NORMAL_TIME = 500;
        private int timeInterval = NORMAL_TIME;

        public TCanvas(int cell_size, int row_cells, int col_cells) {
            cellSize = cell_size;
            rowCells = row_cells;
            colCells = col_cells;
            data = new int[rowCells][colCells];
            this.setBounds(10,10, col_cells*cell_size+1, row_cells*cell_size+1);
            this.appendNewTFigure();
        }

        @Override
        protected void paintComponent(Graphics g) {
            drawBackground(g, Color.white);
            drawData(g);
            if(!gameIsOver) {
                drawCurrentTFigure(g);
            }
            drawGrid(g);
        }

        public void appendNewTFigure() {
            xFigureOffset = 0;
            yFigureOffset = 0;
            currentTFigure = new TFigure();
        }

        private void drawBackground(Graphics g, Color color) {
            g.setColor(color);
            g.fillRect(0,0,getWidth(), getHeight());
        }
        private void drawGrid(Graphics g) {
            g.setColor(Color.BLACK);
            for (int i = 0; i < getWidth(); i+=cellSize) {
                g.drawLine(i, 0, i, getHeight());
            }
            for (int i = 0; i < getHeight(); i+=cellSize) {
                g.drawLine(0, i, getWidth(), i);
            }
        }
        private void drawData(Graphics g) {
            g.setColor(Color.RED);
            for (int i = 0; i < data.length; i++) {
                for (int j = 0; j < data[i].length; j++) {
                    if(data[i][j] == FILLED) {
                        g.fillRect(j * cellSize, i * cellSize, cellSize, cellSize);
                    }
                }
            }
        }
        private void drawCurrentTFigure(Graphics g) {
            g.setColor(Color.CYAN);
            for (int i = 0; i < currentTFigure.getCurrentDataBlock().length; i++) {
                for (int j = 0; j < currentTFigure.getCurrentDataBlock()[i].length; j++) {
                    if(currentTFigure.getCurrentDataBlock()[i][j] == FILLED) {
                        g.fillRect((xFigureOffset + j) * cellSize, (yFigureOffset + i) * cellSize, cellSize, cellSize);
                    }
                }
            }
        }

        public void setPosition(int x, int y) {
            this.setBounds(x, y, colCells*cellSize+1, rowCells*cellSize+1);
        }
        public void keyTyped(KeyEvent e) {

        }
        public void keyPressed(KeyEvent e) {
            if(!gameIsOver) {
                if (e.getKeyCode() == KeyEvent.VK_Q) {
                    tryRotateLeft();
                }
                if(e.getKeyCode() == KeyEvent.VK_E) {
                    tryRotateRight();
                }
                if (e.getKeyCode() == KeyEvent.VK_A) {
                    tryMoveLeft();
                }
                if (e.getKeyCode() == KeyEvent.VK_D) {
                    tryMoveRight();
                }
                if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                    tryRotateRight();
                }
                if (e.getKeyCode() == KeyEvent.VK_S) {
                    timeInterval = LOWER_TIME;
                }
            }
        }
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_S) {
                timeInterval = NORMAL_TIME;
            }
        }

        private boolean canBeHere(int x_fig_offs, int y_fig_offs, int[][] fig_data) {
            boolean can = true;
            for (int i = 0; i < fig_data.length; i++) {
                for (int j = 0; j < fig_data[i].length; j++) {
                    if (fig_data[i][j]==FILLED) {
                        if ((j+x_fig_offs >= colCells)  || (j+x_fig_offs < 0) || (i+y_fig_offs >= rowCells)) {
                            can = false;
                        } else if (data[i+y_fig_offs][j+x_fig_offs] == FILLED) {
                            can = false;
                        }
                    }
                }
            }
            return can;
        }


        private void tryMoveRight() {
            if (canBeHere(xFigureOffset+1, yFigureOffset, currentTFigure.getCurrentDataBlock())) {
                xFigureOffset++;
            }
            repaint();
        }
        private void tryMoveLeft() {
            if (canBeHere(xFigureOffset-1, yFigureOffset, currentTFigure.getCurrentDataBlock())) {
                xFigureOffset--;
            }
            repaint();
        }
        private void tryMoveDown() {
            if (canBeHere(xFigureOffset, yFigureOffset+1, currentTFigure.getCurrentDataBlock())) {
                yFigureOffset++;
            } else {
                addCurrentDataBlockToMainData();
                tryClearLines();
                timeInterval = NORMAL_TIME;

                xFigureOffset = 0;
                yFigureOffset = 0;
                currentTFigure = new TFigure();
                if(!canBeHere(xFigureOffset, yFigureOffset, currentTFigure.getCurrentDataBlock())) {
                    gameOver();
                }
            }
        }

        private void tryClearLines() {
            int[][] buf_data = new int[rowCells][colCells];
            boolean clear_this_line;

            int buf_index = rowCells-1;
            for (int i = rowCells-1; i >= 0; i--) {
                clear_this_line = true;
                for (int j = 0; j < colCells; j++) {
                    if(data[i][j] == FILLED) {
                        buf_data[buf_index][j] = FILLED;
                    } else {
                        buf_data[buf_index][j] = EMPTY;
                        clear_this_line = false;
                    }
                }
                if (!clear_this_line) {
                    buf_index--;
                }
            }
            data = buf_data;
        }

        private void gameOver() {
            System.out.println("GAME IS OVER");
            gameIsOver = true;
            addCurrentDataBlockToMainData();
            currentTFigure = null;
        }

        private void addCurrentDataBlockToMainData() {
            for (int i = 0; i < currentTFigure.getCurrentDataBlock().length; i++) {
                for (int j = 0; j < currentTFigure.getCurrentDataBlock()[i].length; j++) {
                    if(currentTFigure.getCurrentDataBlock()[i][j] == FILLED) {
                        data[i+yFigureOffset][j+xFigureOffset] = FILLED;
                    }
                }
            }
        }

        private void tryRotateRight() {
//            for (int i = -1; i < 2; i++) {
//                if (canBeHere(xFigureOffset+i, yFigureOffset, currentTFigure.peekNextRight())) {
//                    currentTFigure.rotateRight();
//                    xFigureOffset += i;
//                    break;
//                }
//            }
            if (canBeHere(xFigureOffset, yFigureOffset, currentTFigure.peekNextRight())) {
                currentTFigure.rotateRight();
            } else if (canBeHere(xFigureOffset-1, yFigureOffset, currentTFigure.peekNextRight())) {
                currentTFigure.rotateRight();
                xFigureOffset -= 1;
            } else if (canBeHere(xFigureOffset+1, yFigureOffset, currentTFigure.peekNextRight())) {
                currentTFigure.rotateRight();
                xFigureOffset += 1;
            }
            repaint();
        }
        private void tryRotateLeft() {
            if (canBeHere(xFigureOffset, yFigureOffset, currentTFigure.peekNextLeft())) {
                currentTFigure.rotateLeft();
            }
            repaint();
        }


        @Override
        public void run() {
            while(!gameIsOver) {
                try {
                    Thread.sleep(timeInterval);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                tryMoveDown();
                repaint();
            }
        }
    }
    class TFigure {
        private int currentDataBlockNumber = 0;
        private int currentFigureNumber = 0;

        private int[][][][] data = {
            {       // L FIGURE
                    {
                            {0,1,0},
                            {0,1,0},
                            {1,1,0}
                    },
                    {
                            {1,0,0},
                            {1,1,1},
                            {0,0,0}
                    },
                    {
                            {0,1,1},
                            {0,1,0},
                            {0,1,0}
                    },
                    {
                            {0,0,0},
                            {1,1,1},
                            {0,0,1}
                    }
                },
                {   // SQUARE FIGURE
                        {
                                {1,1},
                                {1,1}
                        }
                },
                {   // |- FIGURE
                        {
                                {0,1,0},
                                {1,1,1},
                                {0,0,0}
                        },
                        {
                                {0,1,0},
                                {0,1,1},
                                {0,1,0}
                        },
                        {
                                {0,0,0},
                                {1,1,1},
                                {0,1,0}
                        },
                        {
                                {0,1,0},
                                {1,1,0},
                                {0,1,0}
                        }
                },
                {   // LZ FIGURE
                        {
                                {1,0,0},
                                {1,1,0},
                                {0,1,0}
                        },
                        {
                                {0,1,1},
                                {1,1,0},
                                {0,0,0}
                        }
                },
                {   // RZ FIGURE
                        {
                                {0,0,1},
                                {0,1,1},
                                {0,1,0}
                        },
                        {
                                {1,1,0},
                                {0,1,1},
                                {0,0,0}
                        }
                },
                {   // | FIGURE
                        {
                                {0,1,0,0},
                                {0,1,0,0},
                                {0,1,0,0},
                                {0,1,0,0}
                        },
                        {
                                {0,0,0,0},
                                {0,0,0,0},
                                {1,1,1,1},
                                {0,0,0,0}
                        }
                }
        };
        public int randInt(int min, int max) {
            Random rand = new Random();
            int randomNum = rand.nextInt((max - min) + 1) + min;
            return randomNum;
        }
        public TFigure() {
            currentFigureNumber = randInt(0, data.length-1);
        }

        public int[][] peekNextRight() {
            if(currentDataBlockNumber+1 < data[currentFigureNumber].length)
                return data[currentFigureNumber][currentDataBlockNumber+1];
            else {
                return data[currentFigureNumber][0];
            }
        }

        public int[][] peekNextLeft() {
            if(currentDataBlockNumber-1 >= 0)
                return data[currentFigureNumber][currentDataBlockNumber-1];
            else {
                return data[currentFigureNumber][data.length-1];
            }
        }

        public int[][] getCurrentDataBlock() {
            return data[currentFigureNumber][currentDataBlockNumber];
        }

        public void rotateRight() {
            if(currentDataBlockNumber+1 < data[currentFigureNumber].length)
                currentDataBlockNumber++;
            else {
                currentDataBlockNumber = 0;
            }
        }

        public void rotateLeft() {
            if(currentDataBlockNumber-1 >= 0)
                currentDataBlockNumber--;
            else {
                currentDataBlockNumber = data[currentFigureNumber].length-1;
            }
        }
    }
}
